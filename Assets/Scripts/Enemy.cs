using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Player player;
    public float speed;
    public float health;
    public float maxHealth;
    public float damage;
    public RuntimeAnimatorController[] animCon;
    public Rigidbody2D target;

    public bool isLive;

    public Rigidbody2D rigid;
    public Collider2D col;
    public SpriteRenderer sp;
    public Animator anim;
    private WaitForFixedUpdate wait;

    void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();
        col = GetComponent<Collider2D>();
        sp = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        wait = new WaitForFixedUpdate();
    }

    void FixedUpdate()
    {
        if (!GameManager.instance.isLive)
            return;

        // 애니메이터 0번째 layer의 Hit 상태
        if (!isLive || anim.GetCurrentAnimatorStateInfo(0).IsName("Hit"))
            return;
        
        // 이동
        Vector2 dirVec = target.position - rigid.position;
        Vector2 nextVec = dirVec.normalized * speed * Time.fixedDeltaTime;
        rigid.MovePosition(rigid.position + nextVec);
        rigid.velocity = Vector2.zero;
    }

    void LateUpdate()
    {
        if (!GameManager.instance.isLive)
            return;

        if (!isLive)
            return;

        sp.flipX = target.position.x > rigid.position.x;

    }

    void OnEnable()
    {
        target = GameManager.instance.player.GetComponent<Rigidbody2D>();
        isLive = true;
        col.enabled = true;
        rigid.simulated = true;
        sp.sortingOrder = 3;
        anim.SetBool("Dead", false);
        health = maxHealth;
    }

    public void Init(SpawnData data)
    {
        anim.runtimeAnimatorController = animCon[data.spriteType];
        speed = data.speed;
        maxHealth = data.health;
        health = data.health;
        damage = data.damage;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Bullet") || !isLive)
            return;

        health -= collision.GetComponent<Bullet>().damage;
        StartCoroutine("KnockBack");

        if (health > 0)
            {
                anim.SetTrigger("Hit");
                AudioManager.instance.PlaySfx(AudioManager.Sfx.Hit);
            }

        // 죽음
        else
        {
            isLive = false;
            col.enabled = false;
            rigid.simulated = false;
            sp.sortingOrder = 2;
            anim.SetBool("Dead", true);
            GameManager.instance.kill++;
            GameManager.instance.GetEnergy();
            //GameManager.instance.GetExp();
            DropExp();

            if (GameManager.instance.isLive)
            {
                AudioManager.instance.PlaySfx(AudioManager.Sfx.Dead);
            }

            // Dead();     // 죽는 애니메이션 x
        }
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        if (health <= 0) {
            isLive = false;
            col.enabled = false;
            rigid.simulated = false;
            sp.sortingOrder = 2;
            anim.SetBool("Dead", true);
            GameManager.instance.kill++;
            DropExp();
            
            AudioManager.instance.PlaySfx(AudioManager.Sfx.Dead);
        }
        
        else {
            anim.SetTrigger("Hit");
            AudioManager.instance.PlaySfx(AudioManager.Sfx.Hit);
        }
    }

    public void DropExp()
    {
        // 경험치 오브젝트 생성
        Instantiate(GameManager.instance.expObject, transform.position, Quaternion.identity).GetComponent<Exp>().expAmount = 5;
        // 경험치 증가량
    }

    IEnumerator KnockBack()
    {
        yield return wait; // 다음 하나의 물리 프레임 딜레이
        Vector3 playerPos = GameManager.instance.player.transform.position;
        Vector3 dirVec = transform.position - playerPos;    // 넉백 방향
        rigid.AddForce(dirVec.normalized * 3, ForceMode2D.Impulse); // 힘=3 주기
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (!GameManager.instance.isLive)
            return;

        if (collision.collider.tag == "Player")
        {
            GameManager.instance.health -= damage;
        }
    }

    void Dead()
    {
        gameObject.SetActive(false);
    }
}
