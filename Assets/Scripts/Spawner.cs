using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform[] spawnPoint;      // 소환 위치 배열
    public SpawnData[] spawnData;       // 적 데이터 배열

    public float levelTime;             // 레벨 경과 시간
    public float waveInterval = 20f;    // 웨이브 간격
    public int enemyPerWave = 5;        // 웨이브당 적 수

    private int level = 0;              // 현재 레벨
    private int currentWave = 0;        // 현재 웨이브 번호
    private float timer = 0;            // 타이머 기반 소환 타이머
    public float spawnInterval = 0;     // 스폰 간격 시간
    private float waveTimer = 0;        // 웨이브 간격 타이머
    private bool isWaveActive = false;  // 웨이브 진행 중인지 여부

    [Header("# Bat Spawn")]
    public GameObject[] batPrefabs;         // 박쥐 프리팹 배열
    private GameObject currnetBatPrefab;    // 웨이브에 사용될 박쥐
    public float batDelayTime = 20f;
    public float minSpawnTime = 1f;
    public float maxSpawnTime = 2f;
    public int batPerWave = 5;          // 한 번에 스폰되는 수
    private Transform player;
    public float spawnRadius = 1f;      // 1자 스폰 방지를 위한 랜덤 범위


    void Awake()
    {
        spawnPoint = GetComponentsInChildren<Transform>();
        levelTime = GameManager.instance.maxGameTime / spawnData.Length;

    }

    void Start()
    {
        player = GameManager.instance.player.transform; // GameManager를 통해 플레이어 참조
        StartCoroutine(InitialSpawnDelay());
    }

    void Update()
    {
        if (!GameManager.instance.isLive)
            return;

        // 타이머 기반 소환
        TimerSpawning();

        // 웨이브 기반 소환
        WaveSpawning();

        /* space 누르면 몹 소환
        if (Input.GetButtonDown("Jump"))
        {
            GameManager.instance.pool.Get(0);
        }
        */
    }

    void Spawn()
    {
        GameObject enemy = GameManager.instance.pool.Get(0);
        // 자신은 제외하기 위해 1부터 시작
        enemy.transform.position = spawnPoint[Random.Range(1, spawnPoint.Length)].position;
        enemy.GetComponent<Enemy>().Init(spawnData[level]);
    }

    void TimerSpawning()
    {
        timer += Time.deltaTime;
        level = Mathf.Min(Mathf.FloorToInt(GameManager.instance.gameTime / levelTime), spawnData.Length - 1);

        if (timer > spawnData[level].spawnTime)
        {
            timer = spawnInterval;
            Spawn(); // 기본 타이머 기반 스폰
        }
    }

    void WaveSpawning()
    {
        waveTimer += Time.deltaTime;

        if (!isWaveActive && waveTimer > waveInterval)
        {
            waveTimer = 0;
            StartCoroutine(SpawnWave());
        }
    }

    IEnumerator SpawnWave()
    {
        isWaveActive = true;
        currentWave++;

        int enemyToSpawn = enemyPerWave + (currentWave - 1) * 2; // 웨이브당 적 수 증가

        Debug.Log(enemyToSpawn);

        for (int i = 0; i < enemyToSpawn; i++)
        {
            Spawn(); // 기존 Spawn() 메서드 재사용
            // yield return new WaitForSeconds(0.5f); // 적 간 간격
        }

        isWaveActive = false;
        yield return null;
    }

    IEnumerator InitialSpawnDelay()
    {
        yield return new WaitForSeconds(batDelayTime);

        ChooseRandomBatPrefab();    // 박쥐 선택
        SpawnBatWave();
        StartCoroutine(SpawnBat());
    }

    void ChooseRandomBatPrefab()
    {
        currnetBatPrefab = batPrefabs[Random.Range(0, batPrefabs.Length)];
    }

    void SpawnBatWave()
    {
        // 랜덤 방향 설정 (플레이어 방향 기준)
        // Vector2 randomDirection = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        Transform spawnPoints = spawnPoint[Random.Range(1, spawnPoint.Length)];

        for (int i = 0; i < batPerWave; i++)
        {
            Vector2 randomOffset = Random.insideUnitCircle * spawnRadius;   // 1자 스폰 방지를 위해 원 안에서 랜덤하게 스폰
            Vector3 spawnPosition = spawnPoints.position + new Vector3(randomOffset.x, randomOffset.y, 0);

            GameObject bat = Instantiate(currnetBatPrefab, spawnPosition, spawnPoints.rotation);
            Bat batScript = bat.GetComponent<Bat>();
            batScript.SetDirection(player.position - bat.transform.position);
            //batPrefab.GetComponent<Bat>().SetDirection(randomDirection);
        }
    }

    IEnumerator SpawnBat()
    {
        while (true)
        {
            float spawnInterval = Random.Range(minSpawnTime, maxSpawnTime);
            yield return new WaitForSeconds(spawnInterval);

            ChooseRandomBatPrefab();    // 웨이브에 쓸 bat 프리팹 선택
            SpawnBatWave();
        }
    }




}

[System.Serializable]
public class SpawnData
{
    public float spawnTime;     // 타이머 기반 소환 간격
    public int spriteType;
    public int health;
    public float speed;
    public float damage;
}


