using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fist : MonoBehaviour
{
    public float fallSpeed = 20f;       // 떨어지는 속도
    public Vector3 playerPos;            // 멈출 위치
    private bool hasReachedTarget = false;
    private CameraShake cameraShake;    // 스크립트 참조
    public GameObject impactEffect;     // 충돌 이펙트

    void Start()
    {
        playerPos = GameManager.instance.player.transform.position;

        cameraShake = Camera.main.GetComponent<CameraShake>();
    }

    void Update()
    {
        if (!hasReachedTarget)
        {
            Vector3 direction = (playerPos - transform.position).normalized;
            transform.position += direction * fallSpeed * Time.deltaTime;

            if (Vector3.Distance(transform.position, playerPos) < 0.1f)
            {
                hasReachedTarget = true;        // 주먹이 목표 도달 시
                fallSpeed = 0f;                 // 멈추도록

                StartCoroutine(cameraShake.Shake());
                //Instantiate(impactEffect, transform.position, Quaternion.identity);

                Destroy(gameObject, 0.5f);
            }
        }
              

    }
}
