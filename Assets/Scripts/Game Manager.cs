using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [Header("# Game Control")]
    public bool isLive;
    public float gameTime;
    public float maxGameTime = 2 * 10f;
    [Header("# Player Info")]
    public int playerId;
    public float health;
    public float maxHealth = 100;
    public int level;
    public int kill;
    public int exp;
    public int[] nextExp = { 3, 5, 10, 100, 150, 210, 280, 360, 450, 550 };
    [Header("# Game Object")]
    public PoolManager pool;
    public GameObject expObject;
    public GameObject ultiFist;
    public Transform ultiPoint;
    public Player player;
    public LevelUp uiLevelUp;
    public Result uiResult;
    public Transform uiJoy;
    public GameObject enemyCleaner;
    [Header("# Energy System")]
    public float energy;
    public float maxEnergy = 100f;
    public float energyPerKill = 5f;
    public float energyPerTime = 1f;
    public float ultiDamage = 50f;

    void Awake()
    {
        instance = this;
        Application.targetFrameRate = 60;

        if (expObject == null)
        {
            expObject = Resources.Load<GameObject>("Prefabs/Exp");
        }
    }

    public void GameStart(int id)
    {
        playerId = id;
        health = maxHealth;

        player.gameObject.SetActive(true);
        uiLevelUp.Select(playerId % 2); // 무기 개수가 늘어나도 기본 무기가 지급될 수 있도록
        Resume();

        AudioManager.instance.PlaySfx(AudioManager.Sfx.Hi);
        AudioManager.instance.PlayBgm(true);
        AudioManager.instance.PlaySfx(AudioManager.Sfx.Select);

        // 임시 (첫번째 무기 시작하자마자 들게)
        // uiLevelUp.Select(0);

    }

    public void GameOver()
    {
        AudioManager.instance.PlaySfx(AudioManager.Sfx.Itai);
        StartCoroutine(GameOverRoutine());
    }

    IEnumerator GameOverRoutine()
    {
        isLive = false;

        yield return new WaitForSeconds(1.8f);

        uiResult.gameObject.SetActive(true);
        uiResult.Lose();

        Stop();

        AudioManager.instance.PlayBgm(false);
        AudioManager.instance.PlaySfx(AudioManager.Sfx.jjah);
    }

    public void GameVictroy()
    {
        StartCoroutine(GameVictroyRoutine());
    }

    IEnumerator GameVictroyRoutine()
    {
        isLive = false;
        enemyCleaner.SetActive(true);

        yield return new WaitForSeconds(1.8f);

        uiResult.gameObject.SetActive(true);
        uiResult.Win();

        Stop();

        AudioManager.instance.PlayBgm(false);
        AudioManager.instance.PlaySfx(AudioManager.Sfx.Win);
    }

    public void GameRetry()
    {
        SceneManager.LoadScene(0);
    }

    public void GameQuit()
    {
        Application.Quit();
    }

    void Update()
    {
        if (!isLive)
            return;

        gameTime += Time.deltaTime;

        if (gameTime > maxGameTime)
        {
            gameTime = maxGameTime;
            GameVictroy();
        }

        // 시간이 지나면 에너지 증가
        energy = Mathf.Min(energy + energyPerTime * Time.deltaTime, maxEnergy);
    }

    public void CollectExp(int amount)
    {
        if (!isLive)
            return;
        AudioManager.instance.PlaySfx(AudioManager.Sfx.getExp);
        exp += amount;

        if (exp >= nextExp[Mathf.Min(level, nextExp.Length - 1)])
        {
            level++;
            exp = 0;
            uiLevelUp.Show();
        }
    }

    /*
    public void GetExp()
    {
        // 게임 종료 후 남은 적 clear 할 때 경험치 들어오지 않게
        if (!isLive)
            return;

        exp++;

        // 레벨이 넘어가면 둘 중 최소값을 선택하도록 = 10렙 경험치통
        if (exp == nextExp[Mathf.Min(level, nextExp.Length - 1)])
        {
            level++;
            exp = 0;
            uiLevelUp.Show();
        }
    }
    */

    public void GetEnergy()
    {
        // 게임 종료 후 남은 적 clear 할 때 에너지 들어오지 않게
        if (!isLive)
            return;

        energy = Mathf.Min(energy + energyPerKill, maxEnergy);
    }

    public void UseUltimate()
    {
        // 필살기 발동 로직
        if (energy >= maxEnergy)
        {
            energy = 0;
            UltimateAttack();
        }
    }
    
    public void UltimateAttack() {
        SpawnFist();
        AudioManager.instance.PlaySfx(AudioManager.Sfx.daedap); // 필살기 사운드
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        float ultiDamage = CalculateUltiDamage();

        foreach (GameObject enemy in enemies)
        {
            Enemy enemyScript = enemy.GetComponent<Enemy>();

            if (enemyScript != null)
            {
                enemyScript.TakeDamage(ultiDamage);
            }

            Bat batScript = enemy.GetComponent<Bat>();
            if (batScript != null)
            {
                batScript.TakeDamage(ultiDamage);
            }
        }
        //enemyCleaner.SetActive(true);
        //enemyCleaner.SetActive(false);
    }

        
    void SpawnFist()
    {
        GameObject fist = Instantiate(ultiFist, ultiPoint.position, Quaternion.identity);
        //Rigidbody2D fistRigidbody = fist.GetComponent<Rigidbody2D>();
        //fistRigidbody.AddForce(Vector2.down * 1000f);
    }

    public float CalculateUltiDamage()
    {
        return GameManager.instance.level + 8f;
    }

    public void Stop()
    {
        isLive = false;
        // 시간 배율
        Time.timeScale = 0;
        uiJoy.localScale = Vector3.zero;
    }

    public void Resume()
    {
        isLive = true;
        // 시간 배율
        Time.timeScale = 1;
        uiJoy.localScale = Vector3.one;
    }
}
