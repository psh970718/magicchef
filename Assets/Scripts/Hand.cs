using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    public bool isLeft;
    public SpriteRenderer spriter;

    SpriteRenderer player;

    Vector3 rightPos = new Vector3(0.2f, -0.35f, 0);           // 총은 0.35 -0.15 0
    Vector3 rightPosReverse = new Vector3(0.1f, -0.35f, 0);   // 총은 -0.35 -0.15 0
    Vector3 leftPos = new Vector3(0.2f, -0.4f, 0);             
    Vector3 leftPosReverse = new Vector3(-0.3f, -0.4f, 0);    
    Quaternion leftRot = Quaternion.Euler(0, 0, -105);
    Quaternion leftRotReverse = Quaternion.Euler(0, 0, -255);
    Quaternion rightRot = Quaternion.Euler(0, 0, 35);
    Quaternion rightRotReverse = Quaternion.Euler(0, 0, -35);

    void Awake()
    {
        player = GetComponentsInParent<SpriteRenderer>()[1];
    }

    void LateUpdate()
    {
        bool isReverse = player.flipX;

        if (isLeft)
        { // 근접무기
            transform.localRotation = isReverse ? leftRotReverse : leftRot;
            transform.localPosition = isReverse ? leftPosReverse : leftPos;
            //spriter.flipY = isReverse;
            spriter.sortingOrder = isReverse ? 9 : 11;
        }
        else
        { // 원거리무기
            transform.localRotation = isReverse ? rightRotReverse : rightRot;
            transform.localPosition = isReverse ? rightPosReverse : rightPos;
            spriter.flipX = isReverse;
            spriter.sortingOrder = isReverse ? 11 : 9;
        }
    }
}