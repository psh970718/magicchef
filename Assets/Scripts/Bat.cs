using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : MonoBehaviour
{
    public float batSpeed = 3f;         // 이동 속도
    public float batHealth = 3f;        // 체력
    public float batDamage = 3f;        // 플레이어에게 주는 피해
    public Vector2 moveDirection;       // 이동 방향
    private Animator anim;
    private RuntimeAnimatorController[] animCon;
    private Rigidbody2D rigid;
    private Collider2D col; 
    private SpriteRenderer sp; 
    private bool isLive = true;
    private WaitForFixedUpdate wait;
    public float batSafetime = 2f;     // bat가 화면에 들어올 때 까지의 안전 시간

    void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();
        col = GetComponent<Collider2D>();
        sp = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        wait = new WaitForFixedUpdate();
    }

    void FixedUpdate()
    {
        // 이동
        Vector2 nextVec = moveDirection * batSpeed * Time.fixedDeltaTime;
        rigid.MovePosition(rigid.position + nextVec);

        // 안전 시간이 지난 후에만 화면 밖을 체크
        if (batSafetime <= 0)
        {
            // 카메라 뷰포트를 사용하여 박쥐가 화면을 벗어났는지 확인
            Vector3 viewPos = Camera.main.WorldToViewportPoint(transform.position);
            if (viewPos.x < 0 || viewPos.x > 1 || viewPos.y < 0 || viewPos.y > 1)
            {
                gameObject.SetActive(false); // 오브젝트 풀링
            }
        }
        else
        {
            batSafetime -= Time.fixedDeltaTime; // 안전 시간 감소
        }
    }
    void OnEnable()
    {
        isLive = true;
        col.enabled = true;
        rigid.simulated = true;
        sp.sortingOrder = 3;
        anim.SetBool("Dead", false);
        batSafetime = 2f;
    }


    public void SetDirection(Vector2 direction)
    {
        moveDirection = direction.normalized; // 이동 방향 설정
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Bullet") || !isLive)
            return;

        batHealth -= collision.GetComponent<Bullet>().damage;
        StartCoroutine("KnockBack");

        if (batHealth > 0)
        {
            anim.SetTrigger("Hit");
            AudioManager.instance.PlaySfx(AudioManager.Sfx.Hit);
        }

        // 죽음
        else
        {
            isLive = false;
            col.enabled = false;
            rigid.simulated = false;
            sp.sortingOrder = 2;
            anim.SetBool("Dead", true);
            GameManager.instance.kill++;
            GameManager.instance.GetEnergy();
            //GameManager.instance.GetExp();
            DropExp();

            if (GameManager.instance.isLive)
            {
                AudioManager.instance.PlaySfx(AudioManager.Sfx.Dead);
            }
        }
    }

    public void TakeDamage(float damage)
    {
        batHealth -= damage;
        if (batHealth <= 0)
        {
            isLive = false;
            col.enabled = false;
            rigid.simulated = false;
            sp.sortingOrder = 2;
            anim.SetBool("Dead", true);
            GameManager.instance.kill++;
            DropExp();

            AudioManager.instance.PlaySfx(AudioManager.Sfx.Dead);
        }

        else
        {
            anim.SetTrigger("Hit");
            AudioManager.instance.PlaySfx(AudioManager.Sfx.Hit);
        }
    }

    public void DropExp()
    {
        // 경험치 오브젝트 생성
        Instantiate(GameManager.instance.expObject, transform.position, Quaternion.identity).GetComponent<Exp>().expAmount = 5;
        // 경험치 증가량
    }

    IEnumerator KnockBack()
    {
        yield return wait; // 다음 하나의 물리 프레임 딜레이
        Vector3 playerPos = GameManager.instance.player.transform.position;
        Vector3 dirVec = transform.position - playerPos;    // 넉백 방향
        rigid.AddForce(dirVec.normalized * 3, ForceMode2D.Impulse); // 힘=3 주기
    }

    void Dead()
    {
        gameObject.SetActive(false);
    }

}
