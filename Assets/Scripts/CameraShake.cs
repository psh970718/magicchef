using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
    public float duration = 0.3f;    // 흔들림 지속 시간
    public float magnitude = 0.2f;   // 흔들림 강도

    public IEnumerator Shake()
    {
        Debug.Log("Shake started");
        Vector3 originalPos = transform.localPosition;
        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            // 무작위 위치 생성
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            // 카메라 위치 변경
            transform.localPosition = new Vector3(originalPos.x + x, originalPos.y + y, originalPos.z);

            // 시간 업데이트
            elapsed += Time.deltaTime;

            yield return null;
        }

        // 원래 위치로 복귀
        transform.localPosition = originalPos;
        Debug.Log("Shake end");
    }
}
