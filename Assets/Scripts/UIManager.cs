using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Button ultimateButton;

    void Start()
    {
        ultimateButton.onClick.AddListener(OnUltimateButtonClick);
    }

    void OnUltimateButtonClick()
    {
        GameManager.instance.UseUltimate();
    }
}
